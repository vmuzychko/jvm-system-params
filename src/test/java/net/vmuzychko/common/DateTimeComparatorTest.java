package net.vmuzychko.common;

import net.vmuzychko.model.JvmSystemParametersRecord;
import org.junit.Test;

import static org.junit.Assert.*;


public class DateTimeComparatorTest {

    private DateTimeComparator comparator = new DateTimeComparator();

    @Test
    public void firstShouldBeEarlie() {
        JvmSystemParametersRecord first = new JvmSystemParametersRecord.Builder().build();
        JvmSystemParametersRecord second = new JvmSystemParametersRecord.Builder().build();
        assertEquals(1, comparator.compare(first, second));
    }

    @Test
    public void secondShouldBeEarlie() {
        JvmSystemParametersRecord first = new JvmSystemParametersRecord.Builder().build();
        JvmSystemParametersRecord second = new JvmSystemParametersRecord.Builder().build();
        assertEquals(-1, comparator.compare(second, first));
    }

    @Test
    public void bothShouldBeEqual() {
        JvmSystemParametersRecord record = new JvmSystemParametersRecord.Builder().build();
        assertEquals(0, comparator.compare(record, record));
    }
}