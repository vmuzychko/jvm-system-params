package net.vmuzychko.service;

import net.vmuzychko.common.RecordJsonSerializer;
import net.vmuzychko.model.JvmSystemParametersRecord;
import org.junit.Test;

import static org.junit.Assert.*;


public class RecordJsonSerializerTest {

    private RecordJsonSerializer serializer = new RecordJsonSerializer();


    @Test
    public void shouldDeserializeCorrectly() {
        JvmSystemParametersRecord currentStateRecord = new JvmSystemParametersRecord.Builder().build();
        byte[] bytes = serializer.serialize(currentStateRecord);

        JvmSystemParametersRecord deserializedRecord = serializer.deserialize(bytes);
        assertTrue(deserializedRecord.equals(currentStateRecord));
    }
}