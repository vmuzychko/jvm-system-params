package net.vmuzychko.rest;

import net.vmuzychko.model.JvmSystemParametersRecord;
import net.vmuzychko.service.JvmSystemParametersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class JvmSystemParametersController {

    @Autowired
    private JvmSystemParametersService jvmSystemParametersService;

    @GetMapping("/")
    public List<JvmSystemParametersRecord> getRecords() {
        return jvmSystemParametersService.getRecords();
    }

    @GetMapping("/current")
    public JvmSystemParametersRecord getCurrentState() {
        return jvmSystemParametersService.getCurrentState();
    }

    @DeleteMapping("/")
    public void clear() {
        jvmSystemParametersService.clear();
    }
}
