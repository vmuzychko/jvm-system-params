package net.vmuzychko.service;

import net.vmuzychko.common.DateTimeComparator;
import net.vmuzychko.common.RecordJsonSerializer;
import net.vmuzychko.model.JvmSystemParametersRecord;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Service
public class JvmSystemParametersService implements InitializingBean {

    public static final String JVM_SYS_PARAMS = "JVM_SYS_PARAMS";

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public void afterPropertiesSet() throws Exception {
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashValueSerializer(new RecordJsonSerializer());
    }

    public List<JvmSystemParametersRecord> getRecords() {

        createAndFlushRecord();

        Map<Integer, JvmSystemParametersRecord> entries = redisTemplate.opsForHash().entries(JVM_SYS_PARAMS);
        ArrayList<JvmSystemParametersRecord> records = new ArrayList<>(entries.values());
        records.sort(new DateTimeComparator());

        return records;
    }

    public JvmSystemParametersRecord getCurrentState() {
        JvmSystemParametersRecord record = createAndFlushRecord();
        return record;
    }

    public void clear() {
        redisTemplate.delete(JVM_SYS_PARAMS);
    }

    private JvmSystemParametersRecord createAndFlushRecord() {
        JvmSystemParametersRecord record = new JvmSystemParametersRecord.Builder().build();
        redisTemplate.opsForHash().put(JVM_SYS_PARAMS, record.hashCode(), record);
        return record;
    }
}
