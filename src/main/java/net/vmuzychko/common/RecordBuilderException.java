package net.vmuzychko.common;


public class RecordBuilderException extends RuntimeException {
    public RecordBuilderException(String message, Throwable cause) {
        super(message, cause);
    }
}
