package net.vmuzychko.common;


import net.vmuzychko.model.JvmSystemParametersRecord;
import java.util.Comparator;

public class DateTimeComparator implements Comparator<JvmSystemParametersRecord> {

    @Override
    public int compare(JvmSystemParametersRecord o1, JvmSystemParametersRecord o2) {
        long firstMilli = Long.parseLong(o1.getTimestamp().get("millis"));
        long secondMilli = Long.parseLong(o2.getTimestamp().get("millis"));
        return (firstMilli > secondMilli) ? -1 : ((firstMilli == secondMilli) ? 0 : 1);
    }
}
