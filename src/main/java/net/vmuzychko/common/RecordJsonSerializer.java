package net.vmuzychko.common;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.vmuzychko.model.JvmSystemParametersRecord;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

import java.io.IOException;

public class RecordJsonSerializer implements RedisSerializer<JvmSystemParametersRecord> {

    private final ObjectMapper objectMapper;

    public RecordJsonSerializer() {
        objectMapper = new ObjectMapper().enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL, JsonTypeInfo.As.PROPERTY);
    }

    @Override
    public byte[] serialize(JvmSystemParametersRecord record) throws SerializationException {
        try {
            return objectMapper.writeValueAsBytes(record);
        } catch (JsonProcessingException e) {
            throw new SerializationException(e.getMessage(), e);
        }
    }

    @Override
    public JvmSystemParametersRecord deserialize(byte[] bytes) throws SerializationException {
        if (bytes == null) {
            return null;
        }
        try {
            return objectMapper.readValue(bytes, JvmSystemParametersRecord.class);
        } catch (IOException e) {
            throw new SerializationException(e.getMessage(), e);
        }
    }
}
