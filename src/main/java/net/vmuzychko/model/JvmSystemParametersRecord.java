package net.vmuzychko.model;


import net.vmuzychko.common.RecordBuilderException;
import org.apache.commons.beanutils.BeanUtils;

import java.io.Serializable;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.RuntimeMXBean;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class JvmSystemParametersRecord implements Serializable {

    private Map<String, String> timestamp;

    private List<String> jvmParams;
    private long jvmUpTime;
    private String jvmName;

    private Map<String, String> heapMemoryUsage;
    private Map<String, String> nonHeapMemoryUsage;
    private int objectPendingFinalizationCount;

    private long totalPhysicalMemorySize;
    private long freePhysicalMemorySize;
    private long totalSwapSpaceSize;
    private long freeSwapSpaceSize;
    private long committedVirtualMemorySize;
    private double processCpuLoad;
    private long processCpuTime;

    public JvmSystemParametersRecord() {
    }

    public JvmSystemParametersRecord(Map<String, String> timestamp,
                                     List<String> jvmParams,
                                     long jvmUpTime,
                                     String jvmName,
                                     Map<String, String> heapMemoryUsage,
                                     Map<String, String> nonHeapMemoryUsage,
                                     int objectPendingFinalizationCount,
                                     long totalPhysicalMemorySize,
                                     long freePhysicalMemorySize,
                                     long totalSwapSpaceSize,
                                     long freeSwapSpaceSize,
                                     long committedVirtualMemorySize,
                                     double processCpuLoad,
                                     long processCpuTime) {
        this.timestamp = timestamp;
        this.jvmParams = jvmParams;
        this.jvmUpTime = jvmUpTime;
        this.jvmName = jvmName;
        this.heapMemoryUsage = heapMemoryUsage;
        this.nonHeapMemoryUsage = nonHeapMemoryUsage;
        this.objectPendingFinalizationCount = objectPendingFinalizationCount;
        this.totalPhysicalMemorySize = totalPhysicalMemorySize;
        this.freePhysicalMemorySize = freePhysicalMemorySize;
        this.totalSwapSpaceSize = totalSwapSpaceSize;
        this.freeSwapSpaceSize = freeSwapSpaceSize;
        this.committedVirtualMemorySize = committedVirtualMemorySize;
        this.processCpuLoad = processCpuLoad;
        this.processCpuTime = processCpuTime;
    }

    public Map<String, String> getTimestamp() {
        return timestamp;
    }

    public List<String> getJvmParams() {
        return jvmParams;
    }

    public long getJvmUpTime() {
        return jvmUpTime;
    }

    public String getJvmName() {
        return jvmName;
    }

    public Map<String, String> getHeapMemoryUsage() {
        return heapMemoryUsage;
    }

    public Map<String, String> getNonHeapMemoryUsage() {
        return nonHeapMemoryUsage;
    }

    public int getObjectPendingFinalizationCount() {
        return objectPendingFinalizationCount;
    }

    public long getTotalPhysicalMemorySize() {
        return totalPhysicalMemorySize;
    }

    public long getFreePhysicalMemorySize() {
        return freePhysicalMemorySize;
    }

    public long getTotalSwapSpaceSize() {
        return totalSwapSpaceSize;
    }

    public long getFreeSwapSpaceSize() {
        return freeSwapSpaceSize;
    }

    public long getCommittedVirtualMemorySize() {
        return committedVirtualMemorySize;
    }

    public double getProcessCpuLoad() {
        return processCpuLoad;
    }

    public long getProcessCpuTime() {
        return processCpuTime;
    }

    public static class Builder {

        public JvmSystemParametersRecord build() {
            com.sun.management.OperatingSystemMXBean operatingSystemMXBean = (com.sun.management.OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
            RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();
            MemoryMXBean memoryMXBean = ManagementFactory.getMemoryMXBean();
            try {
                Map<String, String> timestamp = BeanUtils.describe(LocalDateTime.now());
                timestamp.put("millis", String.valueOf(System.currentTimeMillis()));

                return new JvmSystemParametersRecord(
                        timestamp,
                        runtimeMXBean.getInputArguments(),
                        runtimeMXBean.getUptime(),
                        runtimeMXBean.getVmName(),
                        BeanUtils.describe(memoryMXBean.getHeapMemoryUsage()),
                        BeanUtils.describe(memoryMXBean.getNonHeapMemoryUsage()),
                        memoryMXBean.getObjectPendingFinalizationCount(),
                        operatingSystemMXBean.getTotalPhysicalMemorySize(),
                        operatingSystemMXBean.getFreePhysicalMemorySize(),
                        operatingSystemMXBean.getTotalSwapSpaceSize(),
                        operatingSystemMXBean.getFreeSwapSpaceSize(),
                        operatingSystemMXBean.getCommittedVirtualMemorySize(),
                        operatingSystemMXBean.getProcessCpuLoad(),
                        operatingSystemMXBean.getProcessCpuTime());
            } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                throw new RecordBuilderException(e.getMessage(), e);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JvmSystemParametersRecord that = (JvmSystemParametersRecord) o;
        return jvmUpTime == that.jvmUpTime &&
                objectPendingFinalizationCount == that.objectPendingFinalizationCount &&
                totalPhysicalMemorySize == that.totalPhysicalMemorySize &&
                freePhysicalMemorySize == that.freePhysicalMemorySize &&
                totalSwapSpaceSize == that.totalSwapSpaceSize &&
                freeSwapSpaceSize == that.freeSwapSpaceSize &&
                committedVirtualMemorySize == that.committedVirtualMemorySize &&
                Double.compare(that.processCpuLoad, processCpuLoad) == 0 &&
                processCpuTime == that.processCpuTime &&
                Objects.equals(timestamp, that.timestamp) &&
                Objects.equals(jvmParams, that.jvmParams) &&
                Objects.equals(jvmName, that.jvmName) &&
                Objects.equals(heapMemoryUsage, that.heapMemoryUsage) &&
                Objects.equals(nonHeapMemoryUsage, that.nonHeapMemoryUsage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(timestamp,
                            jvmParams,
                            jvmUpTime,
                            jvmName,
                            heapMemoryUsage,
                            nonHeapMemoryUsage,
                            objectPendingFinalizationCount,
                            totalPhysicalMemorySize,
                            freePhysicalMemorySize,
                            totalSwapSpaceSize,
                            freeSwapSpaceSize,
                            committedVirtualMemorySize,
                            processCpuLoad,
                            processCpuTime);
    }
}
