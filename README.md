# README #

Requirements: Java 8, Redis

### What is this repository for? ###

* Service displays either current parameters of JVM and operating system or list of previous parameters collected in Redis. You can clear old params.
* Version 1.0-SNAPSHOT

### rest mapping ###

GET "/"
all records in descending order by timestamp including latest record

GET "/current"
latest record

DELETE "/"
clear all records

